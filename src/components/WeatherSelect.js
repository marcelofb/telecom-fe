import React, { Component } from 'react';
import Select from 'react-select';

const options = [
    { value: 'current', label: 'Ubicación actual' },
    { value: 'cordoba,ar', label: 'Córdoba' },
    { value: 'mendoza,ar', label: 'Mendoza' },
    { value: 'posadas,ar', label: 'Posadas' },
    { value: 'rawson,ar', label: 'Rawson' },
    { value: 'salta,ar', label: 'Salta' },
];

const WEATHER_API_KEY = '21607e0b394183778bfee5bb7fc3bc6e';

export default class WeatherSelect extends Component {
    state = {
        loading: false,
    }

    _executeFetch = async (url) => {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }

    getWeather = async (option) => {
        this.setState({
            loading: true
        })

        let location = option;

        if (location === 'current') {
            const response = await fetch('http://ip-api.com/json/');
            const data = await response.json();
            location = `${data.city},${data.countryCode}`
        }

        const CURRENT_API_URL = `http://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${WEATHER_API_KEY}&units=metric`;
        let current = await this._executeFetch(CURRENT_API_URL);

        const DAILY_FORECAST_API_URL = `http://api.openweathermap.org/data/2.5/forecast/daily?q=${location}&cnt=6&appid=${WEATHER_API_KEY}&units=metric`;
        let dailyForecast = await this._executeFetch(DAILY_FORECAST_API_URL);

        this.setState({
            loading: false
        })

        this.props.onResults(current, dailyForecast)
    }

    render() {
        return(
            <Select
                autoFocus
                isLoading={this.state.loading}
                isSercheable
                noOptionsMessage={() => "No hay resultados"}
                onChange={(option) => this.getWeather(option.value)}
                options={options}
                placeholder="Seleccionar localidad"
                theme={(theme) => ({
                    ...theme,
                    colors: {
                        ...theme.colors,
                        primary25: 'lightgrey',
                        primary: 'black',
                    },
                })}
            />
        )
    }
}