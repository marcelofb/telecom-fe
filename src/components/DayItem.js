import React from 'react'
import moment from 'moment'
import 'moment/locale/es';

const _weatherIcon = {
    Thunderstorm: "wi-thunderstorm",
    Drizzle: "wi-sleet",
    Rain: "wi-storm-showers",
    Snow: "wi-snow",
    Atmosphere: "wi-fog",
    Clear: "wi-day-sunny",
    Clouds: "wi-day-fog"
};

function _getWeatherIcon(rangeId) {
    switch (true) {
        case rangeId >= 200 && rangeId <= 232:
            return _weatherIcon.Thunderstorm
        case rangeId >= 300 && rangeId <= 321:
            return _weatherIcon.Drizzle
        case rangeId >= 500 && rangeId <= 531:
            return _weatherIcon.Rain
        case rangeId >= 600 && rangeId <= 622:
            return _weatherIcon.Snow
        case rangeId >= 701 && rangeId <= 781:
            return _weatherIcon.Atmosphere
        case rangeId === 800:
            return _weatherIcon.Clear
        case rangeId >= 801 && rangeId <= 804:
            return _weatherIcon.Clouds
        default:
            return _weatherIcon.Clouds
    }
}

const DayItem = props => {
    moment.locale('es')
    const { day } = props

    return (
        <div className="day-item">
            {day.main
                ? <div>
                    <i className={`wi ${_getWeatherIcon(day.weather[0].id)} big-icon`}></i>
                    <h3><strong>{Math.floor(day.main.temp)}&deg;</strong></h3>
                  </div>
                : <div>
                    <h5 className="day-name">{moment.unix(day.dt).format("dddd")}</h5>
                    <i className={`wi ${_getWeatherIcon(day.weather[0].id)} small-icon`}></i>
                  </div>
            }
            <h6 className="pt-4">
                <span className="px-2">Min: <strong>{Math.floor(day.temp?.min || day.main.temp_min)}&deg;</strong></span>
                <span className="px-2">Máx: <strong>{Math.floor(day.temp?.max || day.main.temp_max)}&deg;</strong></span>
            </h6>
        </div>
    )
}

export default DayItem