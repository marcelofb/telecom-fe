import React from 'react';
import DayItem from './DayItem'

const WeatherInfo = props => {
    const { days, current } = props

    return (
        <div className="weather-info">
            {current
                ? <div>
                    <DayItem day={current} />
                  </div>
                : null}
            {days 
                ? <div className="row pt-4">
                    {days.list.map((day, index) => {
                        return index !== 0
                            ? <div key={index} className="col-auto mx-auto">
                                <DayItem day={day} />
                              </div>
                            : null
                    })}
                  </div>
                : null}
        </div>
    )
}

export default WeatherInfo;