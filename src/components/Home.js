import React, { Component } from 'react';
import WeatherSelect from './WeatherSelect'
import WeatherInfo from './WeatherInfo'

export default class Home extends Component {
    state = {
        location: undefined,
        current: undefined,
        dailyForest: undefined
    }

    _handleResults = (current, dailyForest) => {
        this.setState({
            location: current.name,
            current,
            dailyForest
        })
    }
    
    render() {
        return(
            <div className="container">
                <div className="row pt-5">
                    <div className="mx-auto select-width">
                        <WeatherSelect onResults={this._handleResults}/>
                    </div>
                </div>
                {this.state.location && <p className="small-text pt-2">Clima actual y extendido para {this.state.location}</p>}
                <div className="row">
                    <div className="col-12">
                        <WeatherInfo days={this.state.dailyForest} current={this.state.current}/>
                    </div>
                </div>
            </div>
        )
    }
}